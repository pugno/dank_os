the dankest OS

requires rust nightly and qemu for development

to run with cargo run:
 - cd into folder
 - rustup override set nightly
 - make sure qemu is installed
 - cargo install bootimage
 - rustup component add llvm-tools-preview
 - cargo bootimage

cargo run should work after this

