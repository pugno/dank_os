#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(dank_os::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;
use bootloader::{entry_point, BootInfo};
use x86_64::VirtAddr;
use core::panic::PanicInfo;
use dank_os::{memory, println, allocator};

entry_point!(kernel_main);

#[no_mangle]
fn kernel_main(boot_info: &'static BootInfo) -> ! {

    println!("Initializing...");
    dank_os::init();

    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { memory::init(phys_mem_offset) };
    let mut frame_allocator = unsafe { memory::BootInfoFrameAllocator::init(&boot_info.memory_map) };

    allocator::init_heap(&mut mapper, &mut frame_allocator)
        .expect("heap initialization failed");

    println!("System checks...");
    #[cfg(test)]
    test_main();

    println!("System OK");
    println!("          ");
    println!("                       .");
    println!("                       M");
    println!("                      dM");
    println!("                      MMr");
    println!("                     4MMML                  .");
    println!("                     MMMMM.                xf");
    println!("     .              \"MMMMM               .MM-");
    println!("      Mh..          +MMMMMM            .MMMM");
    println!("      .MMM.         .MMMMML.          MMMMMh");
    println!("       )MMMh.        MMMMMM         MMMMMMM");
    println!("        3MMMMx.     'MMMMMMf      xnMMMMMM\"");
    println!("        '*MMMMM      MMMMMM.     nMMMMMMP\"");
    println!("          *MMMMMx    \"MMMMM\\    .MMMMMMM=");
    println!("           *MMMMMh   \"MMMMM\"   JMMMMMMP");
    println!("             MMMMMM   3MMMM.  dMMMMMM            .");
    println!("              MMMMMM  \"MMMM  .MMMMM(        .nnMP\"");
    println!("  =..          *MMMMx  MMM\"  dMMMM\"    .nnMMMMM*");
    println!("    \"MMn...     'MMMMr 'MM   MMM\"   .nMMMMMMM*\"");
    println!("     \"4MMMMnn..   *MMM  MM  MMP\"  .dMMMMMMM\"\"");
    println!("       ^MMMMMMMMx.  *ML \"M .M*  .MMMMMM**\"");
    println!("          *PMMMMMMhn. *x > M  .MMMM**\"\"");
    println!("             \"\"**MMMMhx/.h/ .=*\"");
    println!("                      .3P\"%....");
    println!("                    nP\"     \"*MMnx");
    println!("                    ");
    println!("                    ");
    println!("DankOS v0.0.1 (x86_64)");

    dank_os::hlt_loop();
}

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    dank_os::hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    dank_os::test_panic_handler(info)
}

#[test_case]
fn trivial_assertion() {
    assert_eq!(1, 1);
}
